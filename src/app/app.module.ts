import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { DashboardComponent } from './master-components/dashboard/dashboard.component';
import { ProductsModule } from './products/products.module';
import { BrandsModule } from './brands/brands.module';
import { CategoriesModule } from './categories/categories.module';
import { RouterModule, Routes } from '@angular/router';
import { UsersModule } from './users/users.module';
import { MasterComponentsModule } from './master-components/master-components.module';
import { ErrorNotFoundComponent } from './master-components/error-not-found/error-not-found.component';
import { SignInComponent } from './master-components/sign-in/sign-in.component';
import { CookieService } from 'ngx-cookie-service';
import { AuthGuard } from './master-services/auth.guard';
import {OrdersModule} from './orders/orders.module';

registerLocaleData(en);

const route: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'sign-in', component: SignInComponent },
  { path: '**', component: ErrorNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    MasterComponentsModule,
    BrandsModule,
    ProductsModule,
    CategoriesModule,
    UsersModule,
    OrdersModule,
    RouterModule.forRoot(route)
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
