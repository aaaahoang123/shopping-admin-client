import { MasterComponentsModule } from './master-components.module';

describe('MasterComponentsModule', () => {
  let masterComponentsModule: MasterComponentsModule;

  beforeEach(() => {
    masterComponentsModule = new MasterComponentsModule();
  });

  it('should create an instance', () => {
    expect(masterComponentsModule).toBeTruthy();
  });
});
