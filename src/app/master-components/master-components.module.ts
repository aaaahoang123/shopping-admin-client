import { NgModule } from '@angular/core';
import {ErrorNotFoundComponent} from './error-not-found/error-not-found.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NavbarItemsComponent} from './navbar-items/navbar-items.component';
import {RouterModule} from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  imports: [
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ErrorNotFoundComponent, DashboardComponent, NavbarItemsComponent, SignInComponent, BreadcrumbComponent],
  exports: [ErrorNotFoundComponent, DashboardComponent, NavbarItemsComponent, SignInComponent, BreadcrumbComponent]
})
export class MasterComponentsModule { }
