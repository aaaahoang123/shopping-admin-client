import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {AuthService} from '../../master-services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  form: FormGroup;
  apiErrors: any = {};

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private notify: NzNotificationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      username: [ null, [ Validators.required ] ],
      password: [ null, [ Validators.required ] ],
      remember: [ true ]
    });
  }

  async submitForm(val): Promise<any> {
    try {
      await this.authService.login(val);
    } catch (e) {
      this.apiErrors = e.error.errors || {};
      console.log(this.apiErrors);
    } finally {
      Object.keys(this.form.controls).forEach(k => {
        this.form.controls[k].markAsPristine();
        this.form.controls[k].updateValueAndValidity();
      });
    }
  }

}
