import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../master-services/auth.service';

@Component({
  selector: '[app-navbar-items]',
  templateUrl: './navbar-items.component.html',
  styleUrls: ['./navbar-items.component.css']
})
export class NavbarItemsComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  signOut() {
    this.authService.logout();
  }

  checkActive(path: string, match: boolean): string {
    if (match) {
      return window.location.pathname.includes(path) ? 'ant-menu-item-selected' : '';
    } else {
      return path === window.location.pathname ? 'ant-menu-item-selected' : '';
    }
  }
}
