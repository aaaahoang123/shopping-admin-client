import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Breadcrumb} from './breadcrumb';
import {Location} from '@angular/common';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumbs;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.router.events.subscribe(ev => {
      if (ev instanceof NavigationEnd) {
        this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
      }
    });
  }

  buildBreadCrumb(route: ActivatedRoute): Array<Breadcrumb> {
    let currentRoute = route,
        url = '';
    while (currentRoute.firstChild) {
      currentRoute = currentRoute.firstChild;
    }
    const pathFromRoot = currentRoute.pathFromRoot;
    const newBreadcrumbs = [];
    pathFromRoot.forEach(r => {
      const label = r.routeConfig ? r.routeConfig.path === ':id' ? r.snapshot.paramMap.get('id') : r.routeConfig.path : 'Home';
      const path = r.routeConfig ? r.routeConfig.path === ':id' ? r.snapshot.paramMap.get('id') : r.routeConfig.path : '';
      url = `${url}${path}/`;
      newBreadcrumbs.push({
        label: label,
        url: url
      });
    });
    return newBreadcrumbs;
  }

  goBack() {
    this.location.back();
  }
}
