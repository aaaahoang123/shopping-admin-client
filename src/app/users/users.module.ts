import { NgModule } from '@angular/core';
import { UserDetailComponent } from './user-detail/user-detail.component';
import {UsersComponent} from './users.component';
import {UsersTableComponent} from './users-table/users-table.component';
import {UsersFormComponent} from './users-form/users-form.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../master-services/auth.guard';
import {MasterComponentsModule} from '../master-components/master-components.module';

const routes: Routes = [
  {
    path: 'users',
    canActivate: [AuthGuard],
    component: UsersComponent,
    children: [
      {path: '', pathMatch: 'full', component: UsersTableComponent},
      {path: 'create', component: UsersFormComponent},
      {path: ':id', component: UserDetailComponent}
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    MasterComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserDetailComponent, UsersComponent, UsersTableComponent, UsersFormComponent]
})
export class UsersModule { }
