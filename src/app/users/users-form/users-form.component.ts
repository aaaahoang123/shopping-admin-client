import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import * as differenceInCalendarDays from 'date-fns/difference_in_calendar_days';
import {Observable, Observer} from 'rxjs';
import {UsersService} from '../users.service';
import {AccountsService} from '../accounts.service';
import {NzNotificationService} from 'ng-zorro-antd';
import {LocationsService} from '../../master-services/locations.service';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  @Input() existedUser: any;
  @Input() existedAccount: any;
  @Input() formMethod: string;
  @Input() target: string;

  form: FormGroup;
  includeAccount = false;
  phoneStart = '+84';
  isSubmitting = false;
  timeout = null;
  formToDisplay: string[] = ['account', 'user'];
  cities = [];
  districts = [];
  wards = [];
  onSelectingCity: boolean;

  userFormConfig = {
    full_name: [ null, [ Validators.required ] ],
    city: [null, [Validators.required]],
    district: [null, [Validators.required]],
    ward: [null, [Validators.required]],
    address: [ null, [ Validators.required ] ],
    email: [null, [Validators.required, Validators.email]],
    phone: [null, [Validators.required, Validators.pattern(/^[0-9]{3,}?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{1,}$/im)]],
    birthday: [null, [Validators.required]],
    gender: [null, [Validators.required, this.enumGender]],
  };
  accountFormConfig = {
    username: [null, [Validators.required,
      Validators.minLength(8),
      Validators.maxLength(50),
      Validators.pattern(/^[a-zA-Z0-9]+$/)], [this.usernameAsyncValidator()]],
    password: [null, [Validators.required, Validators.minLength(8), Validators.pattern(/^[^\s]+$/)]],
    type: [null, [Validators.required, this.enumType]],
    passwordConfirm: [null, []],
    user: []
  };

  constructor(
    private fb: FormBuilder,
    private userService: UsersService,
    private accountService: AccountsService,
    private notify: NzNotificationService,
    private locationsService: LocationsService
  ) {}

  async ngOnInit() {
    const getCitiesPromise = this.locationsService.getAllCities();
    this.form = this.fb.group({...this.userFormConfig, ...this.accountFormConfig});
    this.form.get('passwordConfirm').setValidators([Validators.required, this.comparePassword()]);
    this.accountFormConfig.passwordConfirm[1] = [Validators.required, this.comparePassword()];
    this.setEnableForm('account', false);

    if (this.target) {
      this.formToDisplay = [this.target];
      if (this.target === 'user') {
        this.form = this.fb.group(this.userFormConfig);
        const phoneArr = this.existedUser.phone.split(' ');
        this.phoneStart = phoneArr[0];

        const getDistrictPromise = this.locationsService.getDistrictsByCity(this.existedUser.city);
        const getWardsPromise = this.locationsService.getWardsByDistrict(this.existedUser.district);

        Object.keys(this.userFormConfig).forEach( k => {
          if (k === 'phone') {
            this.form.get(k).setValue(this.existedUser.phone.replace(this.phoneStart + ' ', ''));
          } else {
            this.form.get(k).setValue(this.existedUser[k]);
          }
          this.form.get(k).markAsDirty();
        });
        this.form.updateValueAndValidity();
        await getWardsPromise;
        this.wards = this.locationsService.wards[this.existedUser.district];
        await getDistrictPromise;
        this.districts = this.locationsService.districts[this.existedUser.city];
      }
      if (this.target === 'account') {
        this.form = this.fb.group(this.accountFormConfig);
        if (this.existedAccount) {
          Object.keys(this.accountFormConfig).forEach(k => {
            if (k === 'passwordConfirm') {
              this.form.get(k).setValue(this.existedAccount.password);
            } else if (k === 'user') {
              this.form.get('user').setValue(this.existedUser._id);
            } else {
              this.form.get(k).setValue(this.existedAccount[k]);
            }
            this.form.get(k).markAsDirty();
          });
          this.form.updateValueAndValidity();
        } else {
          this.form.get('user').setValue(this.existedUser._id);
        }
      }
    }

    await getCitiesPromise;
    this.cities = this.locationsService.cities;
  }

  enumGender(control: AbstractControl): {[key: string]: any} | null {
    return ![0, 1, 2].includes(control.value) ? {'enum': true, error: true} : null;
  }

  enumType(control: AbstractControl): {[key: string]: any} | null {
    return ![1, 2].includes(control.value) ? {'enum': true, error: true} : null;
  }

  onChangeIncludeAccountStatus() {
    // this.includeAccount = !this.includeAccount;

    this.setEnableForm('account', this.includeAccount);
  }

  disabledDate(current: Date): boolean {
    // Can not select days before today and today
    return differenceInCalendarDays(current, new Date()) > 0;
  }

  comparePassword() {
    return (control: AbstractControl) => {
      return control.value !== this.form.get('password').value ? {'compare': true, error: true} : null;
    };
  }

  usernameAsyncValidator () {
    return (control: FormControl) => {
      return Observable.create((observer: Observer<ValidationErrors>) => {
        if (this.timeout !== null) {
          clearTimeout(this.timeout);
          this.timeout = null;
        }
        if (this.existedAccount && this.existedAccount.username === control.value) {
          observer.next(null);
          return observer.complete();
        }

        this.timeout = setTimeout(() => {
          this.accountService.getOne(control.value)
            .then(() => {
              observer.next({ error: true, duplicated: true });
              observer.complete();
            })
            .catch(() => {
              observer.next(null);
              observer.complete();
            });
        }, 500);
      });
    };
  }

  async submitForm(): Promise<any> {
    if (this.form.errors) {
      this.notify.create('warning', 'Can not submit form!', 'You are not complete the form validate by some how!');
      return;
    }
    this.isSubmitting = true;
    switch (this.formMethod) {
      case 'PUT':
        if (this.target === 'user') {
          await this.doPutUser();
        } else {
          await this.doPutAccount();
        }
        break;
      case 'POST':
        await this.doPostAccount();
        break;
      default:
        await this.doPost();
        break;
    }
  }

  async doPost() {
    try {
      const val = this.form.value;
      val.phone = `${this.phoneStart} ${val.phone}`;
      console.log(await this.userService.post(val, {lookup: this.includeAccount}));
      this.resetForm();
      this.notify.create('success', 'Add user success!', 'You can add another user!');
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Add user failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  async doPutUser() {
    try {
      const val = this.form.value;
      val.phone = `${this.phoneStart} ${val.phone}`;
      const newUserData = (<any>await this.userService.put(this.existedUser.user_code, val)).data.attribute;
      this.userService.multiData = this.userService.multiData.map(user => {
        if (user._id === newUserData._id) {
          return {...user, ...newUserData};
        }
        return user;
      });
      this.notify.create('success', 'Edit user success!', 'Check again in list!');
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Edit user failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  async doPostAccount() {
    try {
      const val = this.form.value;
      const newAccount = (<any>await this.accountService.post(val)).data.attribute;
      this.userService.multiData = this.userService.multiData.map(user => {
        if (user._id === newAccount.user) {
          user.account = newAccount;
        }
        return user;
      });
      this.notify.create('success', 'Add account success!', 'Thank you!');
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Add account failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  async doPutAccount() {
    try {
      const val = this.form.value;
      const newAccount = (<any>await this.accountService.put(this.existedAccount._id, val)).data.attribute;
      this.userService.multiData = this.userService.multiData.map(user => {
        if (user._id === newAccount.user) {
          user.account = newAccount;
        }
        return user;
      });
      this.notify.create('success', 'Edit account success!', 'Check again in list!');
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Edit account failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  resetForm(e: MouseEvent = null) {
    if (e) {
      e.preventDefault();
    }
    this.form.reset();
    Object.keys(this.form.controls).forEach(i => {
      this.form.get(i).markAsPristine();
      this.form.get(i).updateValueAndValidity();
    });
    this.includeAccount = false;
    this.onChangeIncludeAccountStatus();
    this.phoneStart = '+84';
  }

  setEnableForm(formName: string, enable: boolean) {
    const func = enable ? 'enable' : 'disable';
    const obj = formName === 'user' ? 'userFormConfig' : 'accountFormConfig';
    Object.keys(this[obj]).forEach(k => {
      if (enable) {
        this.form.get(k).setValidators(this[obj][k][1]);
        if (this[obj][k][2]) {
          this.form.get(k).setAsyncValidators(this[obj][k][2]);
        }
      } else {
        this.form.get(k).setValue(null);
        this.form.get(k).clearValidators();
        this.form.get(k).clearAsyncValidators();
      }
      this.form.get(k).markAsPristine();
      this.form.get(k)[func]();
      this.form.get(k).updateValueAndValidity();
    });
  }

  async onSelectedCity(ev) {
    this.onSelectingCity = true;
    const getDistrictPromise = this.locationsService.getDistrictsByCity(this.form.get('city').value);
    this.form.get('district').setValue(null);
    this.form.get('ward').setValue(null);
    this.wards = [];
    await getDistrictPromise;
    this.districts = this.locationsService.districts[this.form.get('city').value];
    this.onSelectingCity = false;
  }

  async onSelectedDistrict() {
    if (!this.onSelectingCity) {
      const getWardsPromise = this.locationsService.getWardsByDistrict(this.form.get('district').value);
      this.form.get('ward').setValue(null);
      await getWardsPromise;
      this.wards = this.locationsService.wards[this.form.get('district').value];
    }
  }

  checkSelectDisabled(field: string = 'districts') {
    return (!this[field] || this[field].length === 0);
  }
}
