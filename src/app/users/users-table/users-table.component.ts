import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {UsersFormComponent} from '../users-form/users-form.component';
import {UsersService} from '../users.service';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {

  allChecked = false;
  allUnchecked = true;
  indeterminate = false;
  tableLoading = false;
  isDeleting = false;
  searchQuery = '';
  data = [];
  meta;

  constructor(
    private userService: UsersService,
    private notification: NzNotificationService,
    private modalService: NzModalService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    if (this.userService.multiData.length !== 0) {
      this.data = this.userService.multiData;
      this.meta = this.userService.metaData;
      this.searchQuery = this.meta.q;
      return;
    }
    this.getListUsers({data: 'list', lookup: true});
  }

  async getListUsers(condition: any = null) {
    this.tableLoading = true;
    this.cd.detectChanges();
    try {
      const res = await this.userService.getList(condition);
      this.data = res.data.attributes;
      this.meta = res.meta || {};
      this.searchQuery = res.meta.q;
    } catch (err) {
      console.log(err);
      if (err.error.errors) {
        Object.keys(err.error.errors).forEach(k => {
          this.notification.create('error', err.error.errors[k].name, err.error.errors[k].message);
        });
      }
    } finally {
      this.tableLoading = false;
    }
  }

  checkAll(e) {
    this.data.forEach(dat => {
      dat.checked = e;
    });
    this.indeterminate = false;
    this.allChecked = e;
    this.allUnchecked = !e;
  }

  checkOne() {
    this.indeterminate = true;
    this.allUnchecked = false;
    this.allChecked = false;
  }

  async onChangePageSize(e) {
    const meta = this.meta.request_query || {};
    meta.limit = e;
    this.getListUsers(meta);
  }

  onPageIndexChange(e) {
    const meta = this.meta.request_query || {};
    meta.page = e;
    this.getListUsers(meta);
  }

  async confirmDelete(user: any) {
    const index = this.data.indexOf(user);
    this.isDeleting = true;
    try {
      await this.userService.delete(user._id, index);
      this.data = this.userService.multiData;
      this.meta = this.userService.metaData;
      this.notification.create('success', 'Delete user success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete user failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = false;
    }
  }

  cancelDelete() {
    console.log('Cancel delete!');
  }

  openEditUserModal(user: any) {
    const modal = this.modalService.create({
      nzTitle: `Edit user ${user.full_name}`,
      nzContent: UsersFormComponent,
      nzComponentParams: {
        existedUser: user,
        formMethod: 'PUT',
        target: 'user'
      },
      nzOnOk: () => this.data = this.userService.multiData,
      nzOnCancel: () => this.data = this.userService.multiData
    });
  }

  openEditAccountModal(data: any) {
    const account = data.account || null;
    const modal = this.modalService.create({
      nzTitle: account ? `Edit account ${account.username}` : `Add account`,
      nzContent: UsersFormComponent,
      nzComponentParams: {
        existedUser: data,
        existedAccount: account,
        formMethod: account ? 'PUT' : 'POST',
        target: 'account'
      },
      nzOnOk: () => this.data = this.userService.multiData,
      nzOnCancel: () => this.data = this.userService.multiData
    });
  }

  searchUsers() {
    const meta = this.meta.request_query || {};
    meta.q = this.searchQuery;
    meta.page = 1;
    this.getListUsers(meta);
  }

  async deleteSelected() {
    this.isDeleting = this.tableLoading = true;
    const ids = [];
    this.data.forEach(d => {
      if (d.checked) {
        ids.push(d._id);
      }
    });
    if (ids.length === 0) {
      this.isDeleting = this.tableLoading = false;
      return this.notification.create('warning', 'Delete users failed!', 'Please select some users first!');
    }
    try {
      await this.userService.deleteMany(ids);
      this.data = this.userService.multiData;
      this.meta = this.userService.metaData;
      this.notification.create('success', 'Delete users success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete users failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = this.tableLoading = false;
    }
  }

}
