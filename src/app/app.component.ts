import {Component, OnInit} from '@angular/core';
import {
  ActivatedRoute,
  ActivationEnd,
  GuardsCheckEnd,
  NavigationEnd,
  NavigationStart, ResolveEnd, ResolveStart,
  Router,
  RouterEvent,
  RoutesRecognized
} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  collapsedMenu = false;
  startChangeRoute = false;
  timeStartChange;
  progressPercent = 0;
  showProgress = false;
  progressStatus = 'active';
  constructor(private router: Router, private activated: ActivatedRoute) {
    this.router.events.subscribe((ev: RouterEvent) => {
      if (ev instanceof NavigationStart) {
        this.startChangeRoute = true;
        this.showProgress = true;
        this.onChangProcessPercent();
      }
      if (ev instanceof NavigationEnd) {
        this.progressStatus = 'success';
        this.progressPercent = 100;
        setTimeout(() => {
          this.startChangeRoute = false;
          this.showProgress = false;
          this.progressPercent = 0;
          this.progressStatus = 'active';
        }, 300);
      }
    });
  }

  onChangProcessPercent() {
    while (this.progressPercent < 95) {
      this.progressPercent++;
    }
  }

  ngOnInit() {
    if (window.innerWidth < 768) {
      this.collapsedMenu = true;
    }
  }
}
