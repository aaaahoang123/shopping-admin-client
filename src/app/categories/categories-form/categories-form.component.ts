import {Component, HostListener, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {NzMessageService, NzNotificationService} from 'ng-zorro-antd';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from '../../master-services/api-url.service';
import {CategoriesService} from '../categories.service';
import {Observable, Observer} from 'rxjs';

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {

  @Input() existedCategory: any;
  @Input() formMethod: string;
  form: FormGroup;
  isSubmitting = false;
  screenWidth: number = window.innerWidth;
  listParents: any = [];
  timeout = null;
  handleSuggest = [];

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private http: HttpClient,
    private apis: ApiUrlService,
    private notification: NzNotificationService,
    private categoriesSv: CategoriesService
  ) { }

  async ngOnInit() {
    this.form = this.fb.group({
      name: [ '', [ Validators.required]],
      handle: ['', [Validators.required, Validators.pattern(/^[\w-_]*$/)], [this.handleAsyncValidator]],
      description   : [ '', [ Validators.required ] ],
      level: [null, [ Validators.required] ],
      parent: [null, []]
    });

    if (this.existedCategory) {
      await this.setupFormWithExisted();
    }
  }

  async setupFormWithExisted() {
    let fetchListParent, fetchCategory;
    try {
      if (this.existedCategory.level !== 1) {
        fetchListParent = this.categoriesSv.getList({level: this.existedCategory.level - 1, noLimit: true}, false);
      }
      fetchCategory = this.categoriesSv.getOne(this.existedCategory.handle);
      this.listParents = fetchListParent ? (await fetchListParent).data.attributes : [];
      this.existedCategory = (await fetchCategory).data.attribute;
      this.form.setValue({
        name: this.existedCategory.name,
        description: this.existedCategory.description,
        handle: this.existedCategory.handle,
        level: this.existedCategory.level,
        parent: this.existedCategory.parent
      });
      this.form.controls.name.markAsDirty();
      this.form.controls.description.markAsDirty();
      this.form.controls.handle.markAsDirty();
      this.form.controls.level.markAsDirty();
      this.form.controls.parent.markAsDirty();
      this.form.updateValueAndValidity();
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Fetch categories failed', 'Please check log and contact with us!');
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  resetForm(e: MouseEvent = null): void {
    if (e) {
      e.preventDefault();
    }
    this.form.reset();
    Object.keys(this.form.controls).forEach(key => {
      this.form.controls[ key ].markAsPristine();
      this.form.controls[ key ].updateValueAndValidity();
    });
    this.listParents = [];
    this.handleSuggest = [];
  }

  submitForm = async ($event, value) => {
    $event.preventDefault();
    this.isSubmitting = true;
    if (this.formMethod === 'PUT') {
      await this.doPut(value);
    } else {
      await this.doPost(value);
    }
  }

  private async doPost(val): Promise<any> {
    try {
      console.log(await this.categoriesSv.post(val));
      this.resetForm();
      this.notification.create('success', 'Add category success!', 'You can click reset to add another category!');
    } catch (e) {
      this.notification.create('error', 'Add category failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  private async doPut(val): Promise<any> {
    try {
      console.log(await this.categoriesSv.put(this.existedCategory._id, val));
      this.notification.create('success', 'Update category success!', 'Please check table again!');
    } catch (e) {
      this.notification.create('error', 'Update category failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  handleAsyncValidator = (control: FormControl) => {
    return Observable.create((observer: Observer<ValidationErrors>) => {
      if (this.timeout !== null) {
        clearTimeout(this.timeout);
        this.timeout = null;
      }
      if (this.existedCategory && this.existedCategory.handle === control.value) {
        observer.next(null);
        return observer.complete();
      }

      this.timeout = setTimeout(() => {
        this.categoriesSv.getOne(control.value)
          .then(res => {
            observer.next({ error: true, duplicated: true });
            observer.complete();
          })
          .catch(err => {
            observer.next(null);
            observer.complete();
          });
      }, 500);
    });
  }

  makeHandleSuggest(ev) {
    let base: string = this.form.controls.name.value;
    if (base !== '') {
      base = base.toLowerCase().replace(/\s/g, '-').replace(/[^A-Za-z0-9-]/g, '');
      this.handleSuggest = [base, `${base}-1`, `${base}-${new Date().getFullYear()}`];
    } else {
      this.handleSuggest = [];
    }
  }

  async levelChange() {
    const lv = this.form.controls.level.value;
    this.form.controls.parent.setValue(null);
    this.form.controls.parent.markAsDirty();
    if (lv === 1) {
      this.listParents = [];
      this.form.controls.parent.clearValidators();
    } else {
      try {
        this.listParents = (await this.categoriesSv.getList({level: lv - 1, noLimit: 'true'}, false)).data.attributes;
        this.form.controls.parent.setValidators(Validators.required);
      } catch (e) {
        this.listParents = [];
        this.notification.create('error', 'Fetch parent failed!', 'There are some problem with internet! Try again later!');
      }
    }
    this.form.controls.parent.updateValueAndValidity();
  }
}
