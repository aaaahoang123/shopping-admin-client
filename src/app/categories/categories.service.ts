import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from '../master-services/api-url.service';
import {ACustomHttpService} from '../master-services/acustom-http-service';
import {AuthService} from '../master-services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends ACustomHttpService {
  constructor(http: HttpClient, apis: ApiUrlService, authService: AuthService) {
    super(http, apis, authService);
    this.apiUrl = this.apis.CategoriesAPI;
  }
}
