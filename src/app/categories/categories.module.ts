import { NgModule } from '@angular/core';
import { CategoriesComponent } from './categories.component';
import { CategoryDetailComponent } from './category-detail/category-detail.component';
import {CategoriesFormComponent} from './categories-form/categories-form.component';
import {CategoriesTableComponent} from './categories-table/categories-table.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {RouterModule, Routes} from '@angular/router';
import {MasterComponentsModule} from '../master-components/master-components.module';
import {AuthGuard} from '../master-services/auth.guard';

const routes: Routes = [
  {
    path: 'categories',
    component: CategoriesComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', pathMatch: 'full', component: CategoriesTableComponent},
      {path: 'create', component: CategoriesFormComponent},
      {path: ':id', component: CategoryDetailComponent}
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    MasterComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CategoriesComponent, CategoryDetailComponent, CategoriesFormComponent, CategoriesTableComponent]
})
export class CategoriesModule { }
