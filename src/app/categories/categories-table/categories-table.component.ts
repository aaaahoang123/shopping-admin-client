import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CategoriesFormComponent} from '../categories-form/categories-form.component';
import {CategoriesService} from '../categories.service';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';

@Component({
  selector: 'app-categories-table',
  templateUrl: './categories-table.component.html',
  styleUrls: ['./categories-table.component.css']
})
export class CategoriesTableComponent implements OnInit {

  allChecked = false;
  allUnchecked = true;
  indeterminate = false;
  tableLoading = false;
  isDeleting = false;
  searchQuery = '';
  selectedLevel = null;
  levelMapping = ['', 'Root', 'Second group', 'Product category'];
  data = [];
  meta;

  constructor(
    private categoriesService: CategoriesService,
    private notification: NzNotificationService,
    private modalService: NzModalService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    if (this.categoriesService.multiData.length !== 0) {
      this.data = this.categoriesService.multiData;
      this.meta = this.categoriesService.metaData;
      this.searchQuery = this.meta.q;
      this.selectedLevel = this.meta.level || null;
      return;
    }
    this.getListCategories();
  }

  async getListCategories(condition: any = null) {
    this.tableLoading = true;
    this.cd.detectChanges();
    try {
      const res = await this.categoriesService.getList({...condition, lookup: true});
      this.data = res.data.attributes;
      this.meta = res.meta;
      this.searchQuery = res.meta.q;
    } catch (err) {
      console.log(err);
      if (err.error.errors) {
        for (const k in err.error.errors) {
          this.notification.create('error', err.error.errors[k].name, err.error.errors[k].message);
        }
      }
    } finally {
      this.tableLoading = false;
    }
  }

  checkAll(e) {
    this.data.forEach(dat => {
      dat.checked = e;
    });
    this.indeterminate = false;
    this.allChecked = e;
    this.allUnchecked = !e;
  }

  checkOne() {
    this.indeterminate = true;
    this.allUnchecked = false;
    this.allChecked = false;
  }

  async onChangePageSize(e) {
    const meta = this.meta.request_query || {}
    meta.limit = e;
    meta.page = 1;
    this.getListCategories(meta);
  }

  onPageIndexChange(e) {
    const meta = this.meta.request_query || {};
    meta.page = e;
    this.getListCategories(meta);
  }

  async confirmDelete(category: any) {
    const index = this.data.indexOf(category);
    this.isDeleting = true;
    try {
      await this.categoriesService.delete(category._id, index);
      this.data = this.categoriesService.multiData;
      this.meta = this.categoriesService.metaData;
      this.notification.create('success', 'Delete category success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete category failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = false;
    }
  }

  cancelDelete() {
    console.log('Cancel delete!');
  }

  openEditModal(category: any) {
    const modal = this.modalService.create({
      nzTitle: `Edit category ${category.name}`,
      nzContent: CategoriesFormComponent,
      nzComponentParams: {
        existedCategory: category,
        formMethod: 'PUT'
      },
      nzOnOk: () => this.data = this.categoriesService.multiData,
      nzOnCancel: () => this.data = this.categoriesService.multiData
    });
  }

  searchCategories() {
    const meta = this.meta.request_query || {};
    meta.q = this.searchQuery;
    meta.page = 1;
    this.getListCategories(meta);
  }

  onInputSearch() {
    this.meta.q = this.searchQuery;
  }

  onSelectedLevel() {
    const meta = this.meta.request_query || {};
    meta.level = this.selectedLevel;
    meta.page = 1;
    this.categoriesService.metaData.level = this.selectedLevel;
    this.getListCategories(meta);
  }

  async deleteSelected() {
    this.isDeleting = this.tableLoading = true;
    const ids = [];
    this.data.forEach(d => {
      if (d.checked) {
        ids.push(d._id);
      }
    });
    if (ids.length === 0) {
      this.isDeleting = this.tableLoading = false;
      return this.notification.create('warning', 'Delete Categories failed!', 'Please select some categories first!');
    }
    try {
      await this.categoriesService.deleteMany(ids);
      this.data = this.categoriesService.multiData;
      this.meta = this.categoriesService.metaData;
      this.notification.create('success', 'Delete categories success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete categories failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = this.tableLoading = false;
    }
  }
}
