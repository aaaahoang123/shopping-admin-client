import {Component, HostListener, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {NzMessageService, NzNotificationService, UploadFile} from 'ng-zorro-antd';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from '../../master-services/api-url.service';
import {Observable, Observer} from 'rxjs';
import {ProductsService} from '../products.service';
import {CategoriesService} from '../../categories/categories.service';
import {BrandsService} from '../../brands/brands.service';
import {CustomUploadImgService} from '../../master-services/custom-upload-img.service';

@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.css']
})
export class ProductsFormComponent implements OnInit {

  @Input() existedProduct: any;
  @Input() formMethod: string;
  form: FormGroup;
  isSubmitting = false;
  timeout = null;
  imagesList = [];
  categoriesList = [];
  brandsList = [];
  previewVisible = false;
  previewImage;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private http: HttpClient,
    private apis: ApiUrlService,
    private notification: NzNotificationService,
    private productsService: ProductsService,
    private categoriesService: CategoriesService,
    private brandsService: BrandsService,
    private uploadImgService: CustomUploadImgService
  ) { }

  async ngOnInit() {
    this.form = this.fb.group({
      name: [ '', [ Validators.required]],
      code: ['', [Validators.required, Validators.pattern(/^[\w-_]*$/)], [this.productCodeAsyncValidator]],
      description   : [ '', [ Validators.required ] ],
      categories: [[], [ Validators.required, Validators.minLength(1)] ],
      brand: [null, [Validators.required]],
      price: [null, [Validators.required, Validators.min(1)]],
      images: [[], [Validators.required, Validators.minLength(1)]],
      specifications: this.fb.array([])
    });

    this.fetchCategories();
    this.fetchBrands();

    if (this.existedProduct) {
      await this.fetchExistedProduct();
    }
  }

  fetchCategories() {
    this.categoriesService
      .getList({level: 1, lookup: true, noLimit: true}, false)
      .then(res => {
        res.data.attributes.forEach(cate => {
          cate.value = cate._id;
          cate.label = cate.name;
          if (!cate.children || cate.children.length === 0) return cate.isLeaf = true;
          cate.children.forEach(cCate => {
            cCate.value = cCate._id;
            cCate.label = cCate.name;
            if (!cCate.children || cCate.children.length === 0) return cCate.isLeaf = true;
            cCate.children.forEach(gcCate => {
              gcCate.value = gcCate._id;
              gcCate.label = gcCate.name;
              gcCate.isLeaf = true;
            });
          });
        });
        this.categoriesList = res.data.attributes;
      })
      .catch(err => {
        console.log(err);
        this.notification.create('error', 'Load categories failed!', 'Please try again later!');
      });
  }

  fetchBrands() {
    this.brandsService
      .getList({noLimit: true}, false)
      .then(res => {
        this.brandsList = res.data.attributes;
      })
      .catch(err => {
        console.log(err);
        this.notification.create('error', 'Load brands failed!', 'Please try again later!');
      });
  }

  async fetchExistedProduct() {
    try {
      this.existedProduct = (await this.productsService.getOne(this.existedProduct.code, false)).data.attribute;
      this.form.setValue({
        name: this.existedProduct.name,
        description: this.existedProduct.description,
        code: this.existedProduct.code,
        categories: this.existedProduct.categories,
        brand: this.existedProduct.brand,
        price: this.existedProduct.price,
        images: this.existedProduct.images,
        specifications: []
      });
      const existedImg = [];
      this.existedProduct.images.forEach(img => {
        existedImg.push({
          uid: -1,
          name: 'xxx.png',
          status: 'done',
          url: img
        });
      });
      this.imagesList = existedImg;
      if (this.existedProduct.specifications) {
        const sp = this.form.controls.specifications as FormArray;
        Object.keys(this.existedProduct.specifications).forEach(k => {
          sp.push(this.fb.group({key: k, value: this.existedProduct.specifications[k]}));
        });
      }
      this.form.controls.name.markAsDirty();
      this.form.controls.description.markAsDirty();
      this.form.controls.code.markAsDirty();
      this.form.controls.categories.markAsDirty();
      this.form.controls.brand.markAsDirty();
      this.form.controls.price.markAsDirty();
      this.form.controls.images.markAsDirty();
      this.form.controls.specifications.markAsDirty();
      this.form.updateValueAndValidity();
    } catch (e) {
      console.log(e);
      this.notification
        .create('error',
          'Fetch product failed!',
          'May be someone have edited this data! Try refresh first! If this cannot resolve, contact with us!');
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.form.reset();
    Object.keys(this.form.controls).forEach(key => {
      this.form.controls[ key ].markAsPristine();
      this.form.controls[ key ].updateValueAndValidity();
    });
  }

  submitForm = async ($event, value) => {
    $event.preventDefault();
    this.isSubmitting = true;
    if (this.formMethod !== 'PUT') {
      await this.doPost(this.getTrueDataToRequest(value));
    } else {
      await this.doPut(this.getTrueDataToRequest(value));
    }
  }

  async doPost(value): Promise<any> {
    try {
      console.log(await this.productsService.post(value));
      this.notification.create('success', 'Add category success!', 'You can click reset to add another category!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Add category failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  async doPut(value): Promise<any> {
    try {
      console.log(await this.productsService.put(this.existedProduct._id, value));
      this.notification.create('success', 'Update product success!', 'You can click reset to add another category!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Update category failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  getTrueDataToRequest(val): any {
    const data = {...val};
    const trueSpec = {};
    for (const sp of data.specifications) {
      if (![null, undefined, ''].includes(sp.key)) {
        console.log(sp);
        trueSpec[sp.key] = sp.value;
      }
    }
    if (Object.keys(trueSpec).length > 0) {
      data.specifications = trueSpec;
    } else {
      delete data.specifications;
    }
    console.log(data);
    return data;
  }

  productCodeAsyncValidator = (control: FormControl) => {
    return Observable.create((observer: Observer<ValidationErrors>) => {
      if (this.timeout !== null) {
        clearTimeout(this.timeout);
        this.timeout = null;
      }
      if (this.existedProduct && this.existedProduct.code === control.value) {
        observer.next(null);
        return observer.complete();
      }

      this.timeout = setTimeout(() => {
        this.productsService.getOne(control.value)
          .then(res => {
            observer.next({ error: true, duplicated: true });
            observer.complete();
          })
          .catch(err => {
            observer.next(null);
            observer.complete();
          });
      }, 500);
    });
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }

  handleRemove = (file: UploadFile) => {
    const listImg: string[] = this.form.controls.images.value.filter(val => {
      return val !== file.response.secure_url;
    });
    this.form.controls.images.setValue(listImg);
    this.form.controls.images.markAsDirty();
    this.form.controls.images.updateValueAndValidity();
    return true;
  }

  handleUpload = async (item: any) => {
    try {
      const url = await this.uploadImgService.nzCustomUploadImg(item);
      this.form.controls.images.setValue([...this.form.controls.images.value, url]);
      this.form.controls.images.markAsDirty();
      this.form.controls.images.updateValueAndValidity();
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Upload Img failed!', 'Try again later!');
    }
  }

  addSpec(ev) {
    ev.preventDefault();
    const sp = this.form.controls.specifications as FormArray;
    sp.push(this.fb.group({
      key: '',
      value: ''
    }));
  }

  removeSpecifications(ev, i) {
    ev.preventDefault();
    const sp = this.form.controls.specifications as FormArray;
    sp.removeAt(i);
    this.form.controls.specifications.updateValueAndValidity();
  }

  getSpecifications() {
    const sp = this.form.controls.specifications as FormArray;
    return sp.controls;
  }
}
