import { NgModule } from '@angular/core';
import {ProductsFormComponent} from './products-form/products-form.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductsComponent } from './products.component';
import {ProductsTableComponent} from './products-table/products-table.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {en_US, NgZorroAntdModule, NZ_I18N} from 'ng-zorro-antd';
import {RouterModule, Routes} from '@angular/router';
import {MasterComponentsModule} from '../master-components/master-components.module';
import {AuthGuard} from '../master-services/auth.guard';

const routes: Routes = [
  {
    path: 'products',
    canActivate: [AuthGuard],
    component: ProductsComponent,
    children: [
      {path: '', component: ProductsTableComponent, pathMatch: 'full'},
      {path: 'create', component: ProductsFormComponent},
      {path: ':id', component: ProductDetailComponent}
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    MasterComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductsComponent, ProductsFormComponent, ProductDetailComponent, ProductsTableComponent],
  exports: [RouterModule],
  providers: [{ provide: NZ_I18N, useValue: en_US }]
})
export class ProductsModule { }
