import { Injectable } from '@angular/core';
import {ACustomHttpService} from '../master-services/acustom-http-service';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from '../master-services/api-url.service';
import {AuthService} from '../master-services/auth.service';
import {NzNotificationService} from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends ACustomHttpService {
  searchedProducts: any = {};

  constructor(http: HttpClient, apis: ApiUrlService, authService: AuthService, private notify: NzNotificationService) {
    super(http, apis, authService);
    this.apiUrl = this.apis.ProductsAPI;
  }

  async searchProduct(query: string) {
    if (/^[ ]*$/.test(query)) {
      query = '_space_';
    }
    if (this.searchedProducts[query]) {
      return this.searchedProducts[query];
    }
    try {
      let q = '';
      if (query !== '_space_') {
        q = query;
      }
      const res: any = await this.http.get(`${this.apiUrl}?q=${q}`, {headers: {authorization: this.authService.token}}).toPromise();
      this.searchedProducts[query] = res.data.attributes;
      return this.searchedProducts[query];
    } catch (e) {
      console.log(e);
      this.notify.error('Search product failed!', 'Can\'t find any product with the keywords!');
      return [];
    }
  }
}
