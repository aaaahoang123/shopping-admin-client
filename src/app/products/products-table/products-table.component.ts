import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ProductsFormComponent} from '../products-form/products-form.component';
import {ProductsService} from '../products.service';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {CategoriesService} from '../../categories/categories.service';
import {BrandsService} from '../../brands/brands.service';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html',
  styleUrls: ['./products-table.component.css']
})
export class ProductsTableComponent implements OnInit {

  allChecked = false;
  allUnchecked = true;
  indeterminate = false;
  tableLoading = false;
  isDeleting = false;
  searchQuery = '';
  selectedCategory;
  selectedBrand;
  timeout = null;
  listCategories = [];
  listBrands = [];
  isLoading = false;
  data = [];
  meta;

  constructor(
    private productsService: ProductsService,
    private notification: NzNotificationService,
    private modalService: NzModalService,
    private categoriesService: CategoriesService,
    private brandsService: BrandsService,
    private cd: ChangeDetectorRef
  ) {}

  async ngOnInit() {
    await this.getListProducts();
  }

  async getListProducts(condition: any = null) {
    this.tableLoading = true;
    this.cd.detectChanges();
    try {
      const res = await this.productsService.getList({...condition, lookup: true});
      this.data = res.data.attributes;
      this.meta = res.meta;
      this.searchQuery = res.meta.q;
    } catch (err) {
      console.log(err);
      if (err.error.errors) {
        Object.keys(err.error.errors).forEach(k => {
          this.notification.create('error', err.error.errors[k].name, err.error.errors[k].message);
        });
      }
    } finally {
      this.tableLoading = false;
    }
  }

  checkAll(e) {
    this.data.forEach(dat => {
      dat.checked = e;
    });
    this.indeterminate = false;
    this.allChecked = e;
    this.allUnchecked = !e;
  }

  checkOne() {
    this.indeterminate = true;
    this.allUnchecked = false;
    this.allChecked = false;
  }

  async onChangePageSize(e) {
    const meta = this.meta.request_query || {};
    meta.limit = e;
    meta.page = 1;
    await this.getListProducts(meta);
  }

  async onPageIndexChange(e) {
    const meta = this.meta.request_query || {};
    meta.page = e;
    await this.getListProducts(meta);
  }

  async confirmDelete(product: any) {
    const index = this.data.indexOf(product);
    this.isDeleting = true;
    try {
      await this.productsService.delete(product._id, index);
      this.data = this.productsService.multiData;
      this.meta = this.productsService.metaData;
      this.notification.create('success', 'Delete product success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete product failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = false;
    }
  }

  cancelDelete() {
    console.log('Cancel delete!');
  }

  openEditModal(product: any) {
    const modal = this.modalService.create({
      nzTitle: `Edit product: ${product.name}`,
      nzContent: ProductsFormComponent,
      nzComponentParams: {
        existedProduct: product,
        formMethod: 'PUT'
      },
      nzOnOk: () => this.data = this.productsService.multiData,
      nzOnCancel: () => this.data = this.productsService.multiData
    });
  }

  searchProducts() {
    const meta = this.meta.request_query || {};
    meta.q = this.searchQuery;
    meta.page = 1;
    this.getListProducts(meta);
  }

  onInputSearch() {
    this.meta.q = this.searchQuery;
  }

  onSearchCategories(val) {
    if (this.timeout !== null) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.timeout = setTimeout(() => {
      this.isLoading = true;
      this.categoriesService.getList({q: val}, false)
        .then(res => {
          this.listCategories = res.data.attributes;
          this.isLoading = false;
        })
        .catch(err => {
          console.log(err);
          this.isLoading = false;
          this.notification.create('error', 'Search categories failed!', 'Please check log and contact with us!');
        });
    }, 500);
  }

  onSearchBrands(val) {
    if (this.timeout !== null) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.timeout = setTimeout(() => {
      this.isLoading = true;
      this.brandsService.getList({q: val}, false)
        .then(res => {
          this.listBrands = res.data.attributes;
          this.isLoading = false;
        })
        .catch(err => {
          console.log(err);
          this.isLoading = false;
          this.notification.create('error', 'Search brands failed!', 'Please check log and contact with us!');
        });
    }, 500);
  }

  onChangeBrand(ev) {
    const meta = this.meta.request_query || {};
    meta.brand = ev;
    meta.page = 1;
    this.getListProducts(meta);
  }

  onchangeCategory(ev) {
    const meta = this.meta.request_query || {};
    meta.category = ev;
    meta.page = 1;
    this.getListProducts(meta);
  }

  async deleteSelected() {
    this.isDeleting = this.tableLoading = true;
    const ids = [];
    this.data.forEach(d => {
      if (d.checked) {
        ids.push(d._id);
      }
    });
    if (ids.length === 0) {
      this.isDeleting = this.tableLoading = false;
      return this.notification.create('warning', 'Delete products failed!', 'Please select some products first!');
    }
    try {
      await this.productsService.deleteMany(ids);
      this.data = this.productsService.multiData;
      this.meta = this.productsService.metaData;
      this.notification.create('success', 'Delete products success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete products failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = this.tableLoading = false;
    }
  }

}
