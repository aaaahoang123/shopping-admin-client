import { Injectable } from '@angular/core';
import {HttpClient, HttpEventType, HttpRequest, HttpResponse} from '@angular/common/http';
import {ApiUrlService} from './api-url.service';

@Injectable({
  providedIn: 'root'
})
export class CustomUploadImgService {
  imageUrl: string;

  constructor(private apis: ApiUrlService, private http: HttpClient) { }

  /**
   * - Custom upload a img
   * - Please check https://github.com/NG-ZORRO/ng-zorro-antd/blob/cab0536d404aadced9092d7ee82dcb7bf0a7d411/src/components/upload/nz-upload-btn.component.ts.
   * From line 169 - 186
   * @param item
   */
  nzCustomUploadImg(item: any): Promise<any> {
    const form = new FormData();
    form.append('file', item.file);
    form.append('upload_preset', this.apis.UploadImgPreset);

    const req = new HttpRequest('POST', this.apis.UploadImgApi, form, {
      reportProgress: true
    });
    return new Promise<any>((s, e) => {
      this.http.request(req).subscribe((event: any) => {
        if (event.type === HttpEventType.UploadProgress) {
          if (event.total > 0) {
            event.percent = event.loaded / event.total * 100;
          }
          item.onProgress(event);
        } else if (event instanceof HttpResponse) {
          item.onSuccess(event.body, event);
          this.imageUrl = (<any>event.body.secure_url);
          s(this.imageUrl);
        }
      }, err => {
        item.onError(err);
        e(err);
      });
    });
  }
}
