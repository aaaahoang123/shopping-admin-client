import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from './api-url.service';
import {NzNotificationService} from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  cities: any;
  districts: any = {};
  wards: any = {};
  selected: any = {};
  constructor(private http: HttpClient, private apis: ApiUrlService, private notify: NzNotificationService) {}

  async getAllCities() {
    try {
      if (this.cities) {
        return;
      }
      this.cities = (<any>await this.http.get(this.apis.CitiesAPI).toPromise()).data.attributes;
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Fetch cities error!', 'Try again later!');
    }
  }

  async getDistrictsByCity(city_id: number = 3) {
    try {
      this.selected.city = city_id;
      if (this.districts[city_id]) {
        return;
      }
      this.districts[city_id] = (<any>await this.http.get(`${this.apis.DistrictsAPI}?city=${city_id}`).toPromise()).data.attributes;
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Fetch districts error!', 'Try again later!');
    }
  }

  async getWardsByDistrict(district_id: number) {
    try {
      this.selected.district = district_id;
      if (this.wards[district_id]) {
        return;
      }
      this.wards[district_id] = (<any>await this.http.get(`${this.apis.WardsAPI}?district=${district_id}`).toPromise()).data.attributes;
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Fetch wards error!', 'Try again later!');
    }
  }
}
