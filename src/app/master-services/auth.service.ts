import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {NzNotificationService} from 'ng-zorro-antd';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from './api-url.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: string = null;
  username: string = null;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    private notify: NzNotificationService,
    private router: Router,
    private cookie: CookieService,
    private http: HttpClient,
    private urls: ApiUrlService
  ) {}

  checkLoggedIn(): boolean {
    const token = this.cookie.get('token');
    if (token) {
      this.token = token;
      this.username = this.cookie.get('username');
      this.isLoggedIn = true;
    }
    return this.isLoggedIn;
  }

  async login(val): Promise<any> {
    const result: any = await this.http.post(this.urls.AuthenticationAPI, val).toPromise();
    this.token = result.data.attribute.token;
    this.username = val.username;
    this.isLoggedIn = true;
    if (val.remember) {
      const expireDate = new Date();
      expireDate.setDate(expireDate.getDate() + 7);
      this.cookie.set('username', this.username, expireDate);
      this.cookie.set('token', this.token, expireDate);
    }
    this.router
      .navigateByUrl(this.redirectUrl || '')
      .then(() => {
        this.notify.create('success', 'Sign In success', 'Please enjoy your time!');
      })
      .catch((e) => {
        console.log(e);
      });
  }

  logout(): void {
    this.http
      .delete(this.urls.AuthenticationAPI, {headers: {authorization: this.token}})
      .subscribe(res => {
        this.router
          .navigateByUrl('sign-in')
          .then(() => {
            this.notify.create('success', 'Good bye!', 'Thank you! Hope to see you later!');
          })
          .catch((e) => {
            console.log(e);
          });
      }, e => {
        console.log(e);
      });
    this.isLoggedIn = false;
    this.username = this.token = null;
    this.cookie.delete('username');
    this.cookie.delete('token');
  }

}
