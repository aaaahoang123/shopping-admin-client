import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from './api-url.service';
import {AuthService} from './auth.service';

export abstract class ACustomHttpService {
  multiData: any[] = [];
  singleData: any = null;
  metaData: any = null;
  apiUrl: string;
  constructor(public http: HttpClient, public apis: ApiUrlService, public authService: AuthService) { }

  getList(condition: any = null, willCache: boolean = true): Promise<any> {
    let api = this.apiUrl + '?';
    if (condition !== null) {
      for (const key in condition) {
        if (key !== 'request_query') {
          api += `${key}=${condition[key]}&`;
        }
      }
    }
    api = api.slice(0, -1);
    const req = this.http.get(api, {headers: {authorization: this.authService.token}}).toPromise();
    if (willCache) {
      req.then((res: any) => {
        console.log(res);
        this.multiData = res.data.attributes;
        this.metaData = {...this.metaData, ...res.meta};
      }).catch(err => 1);
    }
    return req;
  }

  getOne(name: string = '', lookup: boolean = true, findById: boolean = false): Promise<any> {
    if (name !== '') {
      let url = `${this.apiUrl}${name}?`;
      if (lookup) {
        url += 'lookup=true&';
      }
      if (findById) {
        url += 'findby=id';
      }
      const req = this.http.get(url, {headers: {authorization: this.authService.token}}).toPromise();
      req.then((res: any) => {
        this.singleData = res.data.attribute;
      }).catch(err => 1);
      return req;
    }
    return new Promise<any>((resolve, rejec) => {
      rejec(new Error('You send a wrong keyword!'));
    });
  }

  post(data: any, condition: any = null): Promise<any> {
    let api = this.apiUrl;
    if (condition !== null) {
      api += '?';
      Object.keys(condition).forEach(k => {
        api += `${k}=${condition[k]}&`;
      });
      api = api.slice(0, -1);
    }
    return this.http.post(api, data, {headers: {authorization: this.authService.token}}).toPromise();
  }

  delete(id: any, index): Promise<any> {
    const req = this.http.delete(`${this.apiUrl}${id}`, {headers: {authorization: this.authService.token}}).toPromise();
    req.then(res => {
      this.multiData.splice(index, 1);
      this.metaData.total_items--;
    }).catch(err => 1);
    return req;
  }

  deleteMany(ids: any[]): Promise<any> {
    const req = this.http.delete(this.apiUrl, {headers: {body: ids, authorization: this.authService.token}}).toPromise();
    req.then(res => {
      const newD = [];
      this.multiData.forEach((b) => {
        if (!ids.includes(b._id)) {
          newD.push(b);
        }
      });
      this.multiData = newD;
      this.metaData.total_items -= ids.length;
    }).catch(err => 1);
    return req;
  }

  async put(id: string, data: any, lookup: boolean = false): Promise<any> {
    let url = `${this.apiUrl}${id}`;
    if (lookup) {
      url += '?lookup=true';
    }
    const req = this.http.put(url, data, {headers: {authorization: this.authService.token}}).toPromise();
    req
      .then((res: any) => {
        this.multiData.forEach(async (b, i) => {
          if (b._id === id) {
            this.multiData[i] =
              (<any>await this.http.get(`${this.apiUrl}${id}?findby=id&lookup=true`, {headers: {authorization: this.authService.token}})
              .toPromise()).data.attribute;
          }
        });
      }).catch(e => 1);
    return req;
  }
}
