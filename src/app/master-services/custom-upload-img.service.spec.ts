import { TestBed, inject } from '@angular/core/testing';

import { CustomUploadImgService } from './custom-upload-img.service';

describe('CustomUploadImgService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomUploadImgService]
    });
  });

  it('should be created', inject([CustomUploadImgService], (service: CustomUploadImgService) => {
    expect(service).toBeTruthy();
  }));
});
