import { Injectable } from '@angular/core';

const API = 'http://localhost:3000';
// remote api
// const API = 'https://shopping-admin-api.herokuapp.com';

@Injectable({
  providedIn: 'root'
})
export class ApiUrlService {

  BrandsAPI = `${API}/brands/`;
  CategoriesAPI = `${API}/categories/`;
  ProductsAPI = `${API}/products/`;
  UserAPI = `${API}/users/`;
  AccountsAPI = `${API}/accounts/`;
  OrdersAPI = `${API}/orders/`;
  AuthenticationAPI = `${API}/authentications/`;
  CitiesAPI = `${API}/locations/cities/`;
  DistrictsAPI = `${API}/locations/districts/`;
  WardsAPI = `${API}/locations/wards/`;
  UploadImgApi = `https://api.cloudinary.com/v1_1/fpt-aptech/image/upload`;
  UploadImgPreset = 'gwq6ik7v';

  constructor() { }
}
