import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BrandsService} from '../brands.service';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {BrandsFormComponent} from '../brands-form/brands-form.component';

@Component({
  selector: 'app-brands-table',
  templateUrl: './brands-table.component.html',
  styleUrls: ['./brands-table.component.css']
})
export class BrandsTableComponent implements OnInit {
  allChecked = false;
  allUnchecked = true;
  indeterminate = false;
  tableLoading = false;
  isDeleting = false;
  searchQuery = '';
  data = [];
  meta;

  constructor(
    private brandSv: BrandsService,
    private notification: NzNotificationService,
    private modalService: NzModalService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    if (this.brandSv.multiData.length !== 0) {
      this.data = this.brandSv.multiData;
      this.meta = this.brandSv.metaData;
      this.searchQuery = this.meta.q;
      return;
    }
    this.getListBrands();
  }

  async getListBrands(condition: any = null) {
    this.tableLoading = true;
    this.cd.detectChanges();
    try {
      const res = await this.brandSv.getList(condition);
      this.data = res.data.attributes;
      this.meta = res.meta;
      this.searchQuery = res.meta.q;
    } catch (err) {
      console.log(err);
      if (err.error.errors) {
        for (const k in err.error.errors) {
          this.notification.create('error', err.error.errors[k].name, err.error.errors[k].message);
        }
      }
    } finally {
      this.tableLoading = false;
    }
  }

  checkAll(e) {
    this.data.forEach(dat => {
      dat.checked = e;
    });
    this.indeterminate = false;
    this.allChecked = e;
    this.allUnchecked = !e;
  }

  checkOne() {
    this.indeterminate = true;
    this.allUnchecked = false;
    this.allChecked = false;
  }

  async onChangePageSize(e) {
    const meta = this.meta.request_query || {};
    meta.limit = e;
    this.getListBrands(meta);
  }

  onPageIndexChange(e) {
    const meta = this.meta.request_query || {};
    meta.page = e;
    this.getListBrands(meta);
  }

  async confirmDelete(brand: any) {
    const index = this.data.indexOf(brand);
    this.isDeleting = true;
    try {
      await this.brandSv.delete(brand._id, index);
      this.data = this.brandSv.multiData;
      this.meta = this.brandSv.metaData;
      this.notification.create('success', 'Delete brand success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete brand failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = false;
    }
  }

  cancelDelete() {
    console.log('Cancel delete!');
  }

  openEditModal(brand: any) {
    const modal = this.modalService.create({
      nzTitle: `Edit brand ${brand.name}`,
      nzContent: BrandsFormComponent,
      nzComponentParams: {
        existedBrand: brand,
        formMethod: 'PUT'
      },
      nzOnOk: () => this.data = this.brandSv.multiData,
      nzOnCancel: () => this.data = this.brandSv.multiData
    });
  }

  searchBrands(e) {
    const meta = this.meta.request_query || {};
    meta.q = this.searchQuery;
    meta.page = 1;
    this.getListBrands(meta);
  }

  async deleteSelected() {
    this.isDeleting = this.tableLoading = true;
    const ids = [];
    this.data.forEach(d => {
      if (d.checked) {
        ids.push(d._id);
      }
    });
    if (ids.length === 0) {
      this.isDeleting = this.tableLoading = false;
      return this.notification.create('warning', 'Delete brands failed!', 'Please select some brands first!');
    }
    try {
      await this.brandSv.deleteMany(ids);
      this.data = this.brandSv.multiData;
      this.meta = this.brandSv.metaData;
      this.notification.create('success', 'Delete brands success!', 'If you want to rollback data, contact with us!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Delete brands failed!', 'Please check log and contact with us!');
    } finally {
      this.isDeleting = this.tableLoading = false;
    }
  }
}
