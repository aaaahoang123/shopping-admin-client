import { NgModule } from '@angular/core';
import { BrandsComponent } from './brands.component';
import { BrandDetailComponent } from './brand-detail/brand-detail.component';
import {BrandsFormComponent} from './brands-form/brands-form.component';
import {BrandsTableComponent} from './brands-table/brands-table.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {en_US, NgZorroAntdModule, NZ_I18N} from 'ng-zorro-antd';
import {RouterModule, Routes} from '@angular/router';
import {MasterComponentsModule} from '../master-components/master-components.module';
import {AuthGuard} from '../master-services/auth.guard';

const routes: Routes = [
  {
    path: 'brands',
    component: BrandsComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', pathMatch: 'full', component: BrandsTableComponent},
      {path: 'create', component: BrandsFormComponent},
      {path: ':id', component: BrandDetailComponent}
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    MasterComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BrandsComponent, BrandDetailComponent, BrandsFormComponent, BrandsTableComponent],
  exports: [RouterModule],
  providers: [{ provide: NZ_I18N, useValue: en_US }]
})
export class BrandsModule { }
