import {Component, HostListener, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Observable, Observer} from 'rxjs';
import {NzMessageService, NzNotificationService, UploadFile} from 'ng-zorro-antd';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from '../../master-services/api-url.service';
import {BrandsService} from '../brands.service';
import {CustomUploadImgService} from '../../master-services/custom-upload-img.service';

@Component({
  selector: 'app-brands-form',
  templateUrl: './brands-form.component.html',
  styleUrls: ['./brands-form.component.css']
})
export class BrandsFormComponent implements OnInit {
  // image to display in upload, default = []
  @Input() existedBrand: any;
  @Input() formMethod: string;
  imagesList = [
    // {
    //   uid: -1,
    //   name: 'xxx.png',
    //   status: 'done',
    //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    // }
  ];
  // link image to display in preview modal
  previewImage = '';
  // condition to display preview modal
  previewVisible = false;
  form: FormGroup;
  screenWidth: number = window.innerWidth;
  isSubmitting = false;

  constructor(
    private fb: FormBuilder,
    private msg: NzMessageService,
    private http: HttpClient,
    private apis: ApiUrlService,
    private brandSv: BrandsService,
    private notification: NzNotificationService,
    private imgService: CustomUploadImgService
  ) {}

  async ngOnInit() {
    this.form = this.fb.group({
      name: [ '', [ Validators.required, Validators.maxLength(50) ], [ this.nameAsyncValidator ] ],
      description   : [ '', [ Validators.required ] ],
      logo: [ '', [ Validators.required] ]
    });

    if (this.existedBrand) {
      this.existedBrand = (await this.brandSv.getOne(this.existedBrand.name)).data.attribute;
      this.form.setValue({
        name: this.existedBrand.name,
        description: this.existedBrand.description,
        logo: this.existedBrand.logo
      });
      this.imagesList.push({
        uid: -1,
        name: 'xxx.png',
        status: 'done',
        url: this.existedBrand.logo
      });
      this.form.controls.name.markAsDirty();
      this.form.controls.description.markAsDirty();
      this.form.controls.logo.markAsDirty();
      this.form.updateValueAndValidity();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  submitForm = async ($event, value) => {
    $event.preventDefault();
    this.isSubmitting = true;
    if (this.formMethod !== 'PUT') {
      await this.doPost(value);
    } else {
      await this.doPut(value);
    }
  }

  private async doPost(val): Promise<any> {
    try {
      console.log(await this.brandSv.post(val));
      Object.keys(this.form.controls).forEach(k => {
        this.form.get(k).setValue(null);
        this.form.get(k).markAsPristine();
        this.form.get(k).updateValueAndValidity();
      });
      this.imagesList = [];
      this.notification.create('success', 'Add brand success!', 'You can click reset to add another brand!');
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Add brand failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  private async doPut(val): Promise<any> {
    try {
      console.log(await this.brandSv.put(this.existedBrand._id, val));
      this.notification.create('success', 'Update brand success!', 'Please check table again!');
    } catch (e) {
      this.notification.create('error', 'Update brand failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.form.reset();
    for (const key in this.form.controls) {
      this.form.controls[ key ].markAsPristine();
      this.form.controls[ key ].updateValueAndValidity();
    }
    this.imagesList = [];
  }

  timeout = null;
  nameAsyncValidator = (control: FormControl) => {
    return Observable.create((observer: Observer<ValidationErrors>) => {
      if (this.timeout !== null) {
        clearTimeout(this.timeout);
        this.timeout = null;
      }
      if (this.existedBrand && this.existedBrand.name === control.value) {
        observer.next(null);
        return observer.complete();
      }

      this.timeout = setTimeout(() => {
        this.brandSv.getOne(control.value)
          .then(res => {
            observer.next({ error: true, duplicated: true });
            observer.complete();
          })
          .catch(err => {
            observer.next(null);
            observer.complete();
          });
      }, 500);
    });
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }

  handleUpload = async (item: any) => {
    try {
      const img = await this.imgService.nzCustomUploadImg(item);
      this.form.controls.logo.setValue(img);
      this.form.controls.logo.markAsDirty();
      this.form.controls.logo.updateValueAndValidity();
    } catch (e) {
      console.log(e);
      this.notification.create('error', 'Upload images failed!', 'Please check log and contact with us!');
    }
  }

  handleRemove = (file: UploadFile) => {
    this.form.controls.logo.setValue('');
    this.form.controls.logo.updateValueAndValidity();
    return true;
  }
}
