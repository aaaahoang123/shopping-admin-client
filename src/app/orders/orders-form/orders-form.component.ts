import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductsService} from '../../products/products.service';
import {NzNotificationService} from 'ng-zorro-antd';
import {LocationsService} from '../../master-services/locations.service';
import {OrdersService} from '../orders.service';

@Component({
  selector: 'app-orders-form',
  templateUrl: './orders-form.component.html',
  styleUrls: ['./orders-form.component.css']
})
export class OrdersFormComponent implements OnInit {
  form: FormGroup;
  productOptions = [];
  chosenProduct: any[] = [];
  dataSet = [1];
  timeout = null;
  cities = [];
  districts = [];
  wards = [];
  onSelectingCity = false;
  isSubmitting = false;

  productConfig = {
    _id: [null, [Validators.required]],
    unit_price: [null, [Validators.required]],
    qty: [null, [Validators.required, Validators.min(1)]]
  };
  constructor(
    private fb: FormBuilder,
    private productsService: ProductsService,
    private notify: NzNotificationService,
    private locationsService: LocationsService,
    private ordersService: OrdersService
  ) { }

  async ngOnInit() {
    const getCitiesPromise = this.locationsService.getAllCities();
    this.form = this.fb.group({
      products: this.fb.array([
        this.fb.group(this.productConfig)
      ]),
      receiver_address: [null, [Validators.required]],
      receiver_city: [null, [Validators.required]],
      receiver_district: [null, [Validators.required]],
      receiver_ward: [null, [Validators.required]],
      receiver_phone: [
        null,
        [
          Validators.required, Validators.pattern(/^[\+]?[(]?[0-9]{2,4}[)]?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{1,}$/im)
        ]
      ],
      receiver_email: [
        null,
        [
          Validators.required,
          Validators.pattern(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)
        ]
      ],
      total_price: [null, []],
    });

    await getCitiesPromise;
    this.cities = this.locationsService.cities;
  }

  getProductsFormArray() {
    const products = this.form.controls.products as FormArray;
    return products.controls;
  }

  addProducts(e) {
    const products = this.form.get('products') as FormArray;
    products.push(this.fb.group(this.productConfig));
  }

  mathTotalPrice(): number {
    const products = this.form.get('products') as FormArray;
    let total = 0;
    for (let i = 0; i < products.length; i++) {
      const product = products.controls[i] as FormGroup;
      total += product.get('unit_price').value * product.get('qty').value;
    }
    this.form.get('total_price').setValue(total);
    return total;
  }

  async onSearchProduct(ev) {
    if (this.timeout !== null) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.timeout = setTimeout(async () => {
      this.productOptions = await this.productsService.searchProduct(ev);
    }, 500);
  }

  onChooseProduct(ev, i) {
    const products = this.form.get('products') as FormArray;
    products.controls[i].get('_id').setValue(ev._id);
    products.controls[i].get('unit_price').setValue(ev.price);
  }

  removeProduct(i) {
    const products = this.form.get('products') as FormArray;
    if (products.controls.length === 1) {
      return this.notify.error('Remove failed!', 'Must have at least one product for an order!');
    }

    products.controls.splice(i, 1);
    this.chosenProduct.splice(i, 1);
  }

  async onSelectedCity(ev) {
    this.onSelectingCity = true;
    const getDistrictPromise = this.locationsService.getDistrictsByCity(this.form.get('receiver_city').value);
    this.form.get('receiver_district').setValue(null);
    this.form.get('receiver_ward').setValue(null);
    this.wards = [];
    await getDistrictPromise;
    this.districts = this.locationsService.districts[this.form.get('receiver_city').value];
    this.onSelectingCity = false;
  }

  async onSelectedDistrict() {
    if (!this.onSelectingCity) {
      const getWardsPromise = this.locationsService.getWardsByDistrict(this.form.get('receiver_district').value);
      this.form.get('receiver_ward').setValue(null);
      await getWardsPromise;
      this.wards = this.locationsService.wards[this.form.get('receiver_district').value];
    }
  }

  resetForm(e: MouseEvent = null): void {
    if (e) {
      e.preventDefault();
    }
    this.form.reset();
    Object.keys(this.form.controls).forEach(i => {
      this.form.get(i).markAsPristine();
      this.form.get(i).updateValueAndValidity();
    });
  }

  async onSubmitForm() {
    if (this.form.errors) {
      return this.notify.create('warning', 'Can not submit form!', 'You are not complete the form validate by some how!');
    }
    this.isSubmitting = true;
    await this.doPost(this.form.value);
  }

  async doPost(val) {
    try {
      console.log(await this.ordersService.post(val));
      this.resetForm();
      this.notify.create('success', 'Add order success!', 'You can add another order!');
    } catch (e) {
      console.log(e);
      this.notify.create('error', 'Add order failed!', 'Please check log and contact with us!');
    } finally {
      this.isSubmitting = false;
    }
  }
}
