import { NgModule } from '@angular/core';
import { OrdersComponent } from './orders.component';
import { OrdersFormComponent } from './orders-form/orders-form.component';
import { OrdersTableComponent } from './orders-table/orders-table.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {MasterComponentsModule} from '../master-components/master-components.module';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../master-services/auth.guard';

const routes: Routes = [
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', component: OrdersTableComponent, pathMatch: 'full'},
      {path: 'create', component: OrdersFormComponent},
      {path: ':id', component: OrderDetailComponent}
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    MasterComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrdersComponent, OrdersFormComponent, OrdersTableComponent, OrderDetailComponent]
})
export class OrdersModule { }
